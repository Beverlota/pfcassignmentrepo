using Google.Cloud.Functions.Framework;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System;

namespace CloudFunction
{
    public class Function : IHttpFunction
    {
        /// <summary>
        /// Logic for your function goes here.
        /// </summary>
        /// <param name="context">The HTTP context, containing the request and the response.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        public async Task HandleAsync(HttpContext context)
        {
             var responseOutput = new {StatusCode = HttpStatusCode.OK, Message="Email was successfully sent"};

            try{
                HttpRequest requestIn = context.Request;
                
                using TextReader reader = new StreamReader(requestIn.Body);
                string text = await reader.ReadToEndAsync();
                Dictionary<string,string> parsedObject = JsonConvert.DeserializeObject< Dictionary<string,string>>(text);

                if(parsedObject == null||parsedObject.Count == 0){
                    responseOutput = new {StatusCode = HttpStatusCode.BadRequest, Message = "Error occured when attempting to send email. RequestBody was empty"};
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(responseOutput));
                }

                string emailContents = parsedObject["emailContents"] ?? "" ;
                string recipient = parsedObject["recipient"]?? "" ;
                string mailGunAPIKey =parsedObject["apiKey"]?? "" ;
                string emailDomainName = parsedObject["emailDomainName"]?? "" ;
                string defaultSender = parsedObject["defaultSender"]?? "" ;
                
                RestClient client = new RestClient("https://api.mailgun.net/v3");
                client.Authenticator =
                    new HttpBasicAuthenticator("api", mailGunAPIKey);
                RestRequest request = new RestRequest();
                request.AddParameter("domain",emailDomainName, ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", defaultSender);
                request.AddParameter("to", recipient);
                request.AddParameter("subject", "Booking Receipt");
                request.AddParameter("html", emailContents);
                request.Method = Method.POST;

                var response = await client.ExecuteAsync(request);
            

                if(!response.IsSuccessful){
                    responseOutput = new {StatusCode = response.StatusCode, Message = "Error occured when attempting to send email. Actual response content: " + response.Content};
                }

                await context.Response.WriteAsync(JsonConvert.SerializeObject(responseOutput));
            }catch(Exception e){
                 responseOutput = new {StatusCode = HttpStatusCode.BadRequest, Message = $"Error occured when attempting to send email. Actual Exception: {e}"};
                await context.Response.WriteAsync(JsonConvert.SerializeObject(responseOutput));
            }

        }
    }
}
