﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Models
{
    public class CreateBookingRequest
    {
        [Required]
        [DisplayName("Pick Up Address")]
        public string PickUpAddress { get; set; }
        [Required]
        [DisplayName("Drop Off Address")]
        public string DropOffAddress { get; set; }
        [Required]
        [DisplayName("Car Category Type")]
        public CarCategory CarType { get; set; }

        [DisplayName("Trip Price")]
        public decimal TripPrice { get; set; }

        public IdentityUser Passenger { get; set; }
    }
}
