﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Models
{
    public class CarCategoryResponse
    {
        [Required]
        [DisplayName("Car Category Type")]
        public CarCategory CarCategoryType { get; set; }
        [Required]
        [DisplayName("Price per km")]
        public decimal Price { get; set; }
    }
}
