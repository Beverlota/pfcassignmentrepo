﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Utilities.Interfaces
{
    public interface ILogHandler
    {
        void Log(string messageContent, Google.Cloud.Logging.Type.LogSeverity severity);
    }
}
