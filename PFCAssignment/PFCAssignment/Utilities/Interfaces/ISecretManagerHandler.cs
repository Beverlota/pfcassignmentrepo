﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Utilities.Interfaces
{
    public interface ISecretManagerHandler
    {
        public string GetSecretValue(string key);
    }
}
