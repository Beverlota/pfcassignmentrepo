﻿using PFCAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Utilities.Interfaces
{
    public interface ICacheHandler
    {
        public Dictionary<int, CreateBookingRequest> GetBookingDataFromCache();
        public void AddEntryToBookingCachedData(KeyValuePair<int, CreateBookingRequest> bookingData);
        public void RemoveEntryFromBookingCachedData(KeyValuePair<int, CreateBookingRequest> bookingData);
    }
}
