﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Utilities.Interfaces
{
    public interface IEmailSender
    {
        public dynamic TriggerCloudFunction(string emailContent, string recipientEmail);
    }
}
