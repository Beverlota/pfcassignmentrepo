﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PFCAssignment.Utilities.Interfaces;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace PFCAssignment.Utilities.Implementations
{
    public class EmailSender : IEmailSender
    {
        private readonly string _emailDomainName;
        private readonly string _defaultEmailSender;
        private ISecretManagerHandler _secretManagerHandler;

        public EmailSender(IConfiguration config, ISecretManagerHandler secretManagerHandler)
        {
            _secretManagerHandler = secretManagerHandler;
            _emailDomainName = config.GetSection("AppSettings").GetSection("MailGun").
                GetSection("DomainName").Value;
            _defaultEmailSender = config.GetSection("AppSettings").GetSection("MailGun").
                GetSection("DefaultSender").Value;
        }

        public dynamic TriggerCloudFunction(string emailContent, string recipientEmail)
        {
            HttpClient client = new HttpClient();

            var values = new Dictionary<string, string>
            {
                {"emailContents",emailContent },
                {"recipient" ,recipientEmail },
                {"apiKey" ,_secretManagerHandler.GetSecretValue("MailGunAPIKey")},
                {"emailDomainName" ,_emailDomainName },
                {"defaultSender" ,_defaultEmailSender },
            };

            var content = JsonContent.Create(values);

            Task<HttpResponseMessage> t = client.PostAsync("https://us-central1-pfcassignment.cloudfunctions.net/pfcAssignmentCloudFunction", content);

            t.Wait();

            var value = t.Result.Content.ReadAsStringAsync();
            value.Wait();

            dynamic output = JsonConvert.DeserializeObject(value.Result);

            return output["Message"]; 
        }
    }

    public class ReceiptTemplate
    {
        public string buildReceiptTemplate(int bookingNo, string username,
            decimal amount)
        {
            string s = "<!DOCTYPE html>" +
            "<html  style = \"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\">" +
            "<head>" +
            "<meta name = \"viewport\" content = \"width=device-width\"/>" +
            "<meta http - equiv = \"Content-Type\" content = \"text/html; charset=UTF-8\"/>" +
            "<title>Receipt</title>" +
            "<style type = \"text/css\">" +
            "img {" +
             "   max - width: 100 %;" +
            " }" +
            "body {" +
             "   -webkit - font - smoothing: antialiased; -webkit - text - size - adjust: none; width: 100 % !important; height: 100 %; line - height: 1.6em;" +
            "}" +
           " body {" +
            "    background - color: #f6f6f6;" +
            "}" +
            "@media only screen and(max-width: 640px) {" +
              "  body {" +
              "  padding: 0!important;" +
              "  }" +
              "  h1 {" +
               "     font - weight: 800!important; margin: 20px 0 5px!important;" +
               " }" +
               " h2 {" +
                 "   font - weight: 800!important; margin: 20px 0 5px!important;}" +
                "h3 {font - weight: 800!important; margin: 20px 0 5px!important;}" +
               " h4 {font - weight: 800!important; margin: 20px 0 5px!important;} " +
               " h1 { font - size: 22px!important;}                               " +
               " h2 {font - size: 18px!important;}                                " +
               " h3 { font - size: 16px!important;}                               " +
               ".container {padding: 0!important; width: 100 % !important; }     " +
               ".content {padding: 0!important;}                                 " +
               ".content - wrap {padding: 10px!important;}                       " +
                ".invoice {width: 100 % !important;}" +
            "}" +
        "</style>" +
        "</head>" +
        "<body itemscope itemtype = \"http://schema.org/EmailMessage\" style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;\" bgcolor = \"#f6f6f6\">"+
        "<table class=\"body-wrap\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;\" bgcolor=\"#f6f6f6\"><tr style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\" valign=\"top\"></td>"+
        "<td class=\"container\" width=\"600\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;\" valign=\"top\">"+
        "<div class=\"content\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;\">"+
        "<table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-wrap aligncenter\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 20px;\" align=\"center\" valign=\"top\">"+
              "<table width = \"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tr style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">"+
                    "<h1 class=\"aligncenter\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; box-sizing: border-box; font-size: 32px; color: #000; line-height: 1.2em; font-weight: 500; text-align: center; margin: 40px 0 0;\" align=\"center\">Booking Receipt</h1>"+
                  "</td>"+
                    "<table class=\"invoice\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; text-align: left; width: 80%; margin: 40px auto;\"><tr style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;\">{0}<br style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"/> Booking Number {1}<br style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"/>{2}</ td>"+
        "</tr><tr style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 5px 0;\" valign = \"top\">"+
        "</tr><tr class= \"total\" style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class= \"alignright\" width = \"80%\" style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 2px; border-top-color: #333; border-top-style: solid; border-bottom-color: #333; border-bottom-width: 2px; border-bottom-style: solid; font-weight: 700; margin: 0; padding: 5px 0;\" align = \"right\" valign = \"top\"> Total </td>"+
        "<td class= \"alignright\" style = \"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 2px; border-top-color: #333; border-top-style: solid; border-bottom-color: #333; border-bottom-width: 2px; border-bottom-style: solid; font-weight: 700; margin: 0; padding: 5px 0;\" align = \"right\" valign = \"top\"> &#8364; {3}</td>"+
        "</tr></table></td>" +
        "</tr></table></td>" +
        "</tr></table></td>" +
        "</tr></table><div class= \"footer\" style = \"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">" +
        "<table width = \"100%\" style = \"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tr style = \"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class= \"aligncenter content-block\" style = \"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;\" align = \"center\" valign = \"top\"> Questions ? Email <a href = \"mailto:\" style = \"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;\"> test@gmail.com </a></td>" +
        "</tr></table></div></div>" +
        "</td>" +
        "<td style = \"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\" valign = \"top\"></td>" +
        "</tr></table></body>" +
        "</html>";

            s = s.Replace("{0}", username);
            s = s.Replace("{1}", bookingNo.ToString());
            s = s.Replace("{2}", DateTime.Now.ToLongDateString() +" "+ DateTime.Now.ToLongTimeString());
            s = s.Replace("{3}", amount.ToString());

            return s;
        }
    }
}
