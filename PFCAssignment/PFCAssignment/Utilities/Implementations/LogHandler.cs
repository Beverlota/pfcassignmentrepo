﻿using Google.Api;
using Google.Cloud.Logging.Type;
using Google.Cloud.Logging.V2;
using Microsoft.Extensions.Configuration;
using PFCAssignment.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Utilities.Implementations
{
    public class LogHandler : ILogHandler
    {
        private readonly string _projectId;
        private readonly string _logId;
        private readonly string _logType;
        private readonly IConfiguration _config;

        public LogHandler(IConfiguration config)
        {
            _config = config;

            _projectId = _config.GetSection("AppSettings").GetSection("ProjectId").Value;
            _logId = _config.GetSection("AppSettings").GetSection("CloudLogging").GetSection("LogId").Value;
            _logType = _config.GetSection("AppSettings").GetSection("CloudLogging").GetSection("LogType").Value;
        }

        public void Log(string messageContent, LogSeverity severity)
        {
            var loggingClient = LoggingServiceV2Client.Create();

            LogName logName = new LogName(_projectId, _logId); 

            MonitoredResource res = new MonitoredResource();
            res.Type = _logType; 

            List<LogEntry> entries = new List<LogEntry>();
            entries.Add(new LogEntry() { LogName = logName.ToString(), Severity = severity, TextPayload = messageContent });

            loggingClient.WriteLogEntries(logName, res, null, entries);
        }
    }
}
