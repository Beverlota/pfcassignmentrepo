﻿using Microsoft.Extensions.Caching.Memory;
using PFCAssignment.Models;
using PFCAssignment.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Utilities.Implementations
{
    public class CacheHandler : ICacheHandler
    {
        private readonly IMemoryCache _cache;
        private readonly ILogHandler _log;

        public CacheHandler(IMemoryCache cache, ILogHandler log)
        {
            _cache = cache;
            _log = log;
        }

        public void AddEntryToBookingCachedData(KeyValuePair<int, CreateBookingRequest> bookingData)
        {
            try
            {
                var currentCache = GetBookingDataFromCache();
                currentCache.Add(bookingData.Key, bookingData.Value);
                _cache.Set("bookingEmailKey", currentCache, TimeSpan.FromMinutes(5));
            }
            catch
            {
                _log.Log("Error occured when attempting to insert item into cache",
                    Google.Cloud.Logging.Type.LogSeverity.Error);
            }
        }

        public Dictionary<int, CreateBookingRequest> GetBookingDataFromCache()
        {
            Dictionary<int, CreateBookingRequest> cacheEntry = null;
            if (!_cache.TryGetValue("bookingEmailKey", out cacheEntry))
            {
                cacheEntry = new Dictionary<int, CreateBookingRequest>();
                _cache.Set("bookingEmailKey", cacheEntry, TimeSpan.FromMinutes(5));
            }

            return cacheEntry;
        }

        public void RemoveEntryFromBookingCachedData(KeyValuePair<int, CreateBookingRequest> bookingData)
        {
            var currentCache = GetBookingDataFromCache();
            bool successfulRemove = currentCache.Remove(bookingData.Key);

            if (!successfulRemove)
                _log.Log($"Error occured when attempting to remove {bookingData.Key}:{bookingData.Value}"
                    , Google.Cloud.Logging.Type.LogSeverity.Error);

            _cache.Set("bookingEmailKey", currentCache, TimeSpan.FromMinutes(5));
        }
    }
}
