﻿using Google.Cloud.SecretManager.V1;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PFCAssignment.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Utilities.Implementations
{
    public class SecretManagerHandler : ISecretManagerHandler
    {
        private readonly IConfiguration _config;
        private readonly SecretManagerServiceClient _client;
        private readonly SecretVersionName _secretVersionName;

        public SecretManagerHandler(IConfiguration config)
        {
            _config = config;

            string secretVersionNo = _config.GetSection("AppSettings").
            GetSection("SecretsDetails").GetSection("CurrentVersion").Value;
            string secretName = _config.GetSection("AppSettings").
                GetSection("SecretsDetails").GetSection("SecretName").Value;
            string projectId = _config.GetSection("AppSettings").
                GetSection("ProjectId").Value;
             
            _client = SecretManagerServiceClient.Create();

            _secretVersionName = new SecretVersionName(projectId, secretName, secretVersionNo);
        }

        public string GetSecretValue(string key)
        {
            AccessSecretVersionResponse result = _client.AccessSecretVersion(_secretVersionName);

            // Convert the payload to a string. Payloads are bytes by default.
            String payload = result.Payload.Data.ToStringUtf8();

            dynamic obj = JsonConvert.DeserializeObject(payload);
            string value = obj[key];

            return value;
        }
    }
}

