#pragma checksum "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0e6ab0c227fa93dd49213de1db0bf201adfc5422"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Bookings_GetAcknowledgedBookings), @"mvc.1.0.view", @"/Views/Bookings/GetAcknowledgedBookings.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\_ViewImports.cshtml"
using PFCAssignment;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\_ViewImports.cshtml"
using PFCAssignment.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0e6ab0c227fa93dd49213de1db0bf201adfc5422", @"/Views/Bookings/GetAcknowledgedBookings.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d58c6d6c07148c64d00117bc2c85c035760d8add", @"/Views/_ViewImports.cshtml")]
    public class Views_Bookings_GetAcknowledgedBookings : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<PFCAssignment.Domain.Booking>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
  
    ViewData["Title"] = "Acknowledged Bookings";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 7 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
 if (ViewBag.Message != null)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class=\"alert alert-success\" role=\"alert\">\r\n        ");
#nullable restore
#line 10 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
   Write(ViewBag.Message);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n");
#nullable restore
#line 12 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 14 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
 if (ViewBag.Error != null)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class=\"alert alert-danger\" role=\"alert\">\r\n        ");
#nullable restore
#line 17 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
   Write(ViewBag.Error);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n");
#nullable restore
#line 19 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Confirmed Bookings</h1>\r\n<hr />\r\n\r\n");
#nullable restore
#line 24 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
 if (Model.Any())
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <table class=\"table\">\r\n        <thead>\r\n            <tr>\r\n                <th>\r\n                    ");
#nullable restore
#line 30 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
               Write(Html.DisplayNameFor(model => model.PickUpAddress));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 33 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
               Write(Html.DisplayNameFor(model => model.DropOffAddress));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 36 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
               Write(Html.DisplayNameFor(model => model.Acknowledged));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 39 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
               Write(Html.DisplayNameFor(model => model.AssignedCarId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 42 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
               Write(Html.DisplayNameFor(model => model.PassengerId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 45 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
               Write(Html.DisplayNameFor(model => model.DriverId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th></th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n");
#nullable restore
#line 51 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
#nullable restore
#line 55 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
                   Write(Html.DisplayFor(modelItem => item.PickUpAddress));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 58 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
                   Write(Html.DisplayFor(modelItem => item.DropOffAddress));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 61 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Acknowledged));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 64 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
                   Write(Html.DisplayFor(modelItem => item.AssignedCarId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 67 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
                   Write(Html.DisplayFor(modelItem => item.PassengerId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 70 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
                   Write(Html.DisplayFor(modelItem => item.DriverId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
#nullable restore
#line 73 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tbody>\r\n    </table>\r\n");
#nullable restore
#line 76 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
}
else
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class=\"alert alert-info\" role=\"alertdialog\">\r\n        There are currently no acknowledged bookings\r\n    </div>\r\n");
#nullable restore
#line 82 "C:\Users\Anonymous\Desktop\Repos\PFCAssignmentRepo\PFCAssignment\PFCAssignment\Views\Bookings\GetAcknowledgedBookings.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<PFCAssignment.Domain.Booking>> Html { get; private set; }
    }
}
#pragma warning restore 1591
