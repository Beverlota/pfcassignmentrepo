﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ProductVersion" character varying(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);


DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210410175042_InitialMigration') THEN
    INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
    VALUES ('20210410175042_InitialMigration', '3.1.13');
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" DROP CONSTRAINT "FK_Bookings_Cars_CarId";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" DROP CONSTRAINT "FK_Bookings_AspNetUsers_DriverId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" DROP CONSTRAINT "FK_Bookings_AspNetUsers_PassengerId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Cars" DROP CONSTRAINT "FK_Cars_AspNetUsers_DriverId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    DROP INDEX "IX_Cars_DriverId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    DROP INDEX "IX_Bookings_CarId";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    DROP INDEX "IX_Bookings_DriverId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    DROP INDEX "IX_Bookings_PassengerId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Cars" DROP COLUMN "DriverId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" DROP COLUMN "CarId";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" DROP COLUMN "DriverId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" DROP COLUMN "PassengerId1";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Cars" ALTER COLUMN "DriverId" TYPE text;
    ALTER TABLE "Cars" ALTER COLUMN "DriverId" DROP NOT NULL;
    ALTER TABLE "Cars" ALTER COLUMN "DriverId" DROP DEFAULT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" ALTER COLUMN "PassengerId" TYPE text;
    ALTER TABLE "Bookings" ALTER COLUMN "PassengerId" DROP NOT NULL;
    ALTER TABLE "Bookings" ALTER COLUMN "PassengerId" DROP DEFAULT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" ALTER COLUMN "DriverId" TYPE text;
    ALTER TABLE "Bookings" ALTER COLUMN "DriverId" DROP NOT NULL;
    ALTER TABLE "Bookings" ALTER COLUMN "DriverId" DROP DEFAULT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" ADD "AssignedCarId" integer NOT NULL DEFAULT 0;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" ADD "CarTypeRequested" integer NOT NULL DEFAULT 0;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "AspNetUserTokens" ALTER COLUMN "Name" TYPE text;
    ALTER TABLE "AspNetUserTokens" ALTER COLUMN "Name" SET NOT NULL;
    ALTER TABLE "AspNetUserTokens" ALTER COLUMN "Name" DROP DEFAULT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "AspNetUserTokens" ALTER COLUMN "LoginProvider" TYPE text;
    ALTER TABLE "AspNetUserTokens" ALTER COLUMN "LoginProvider" SET NOT NULL;
    ALTER TABLE "AspNetUserTokens" ALTER COLUMN "LoginProvider" DROP DEFAULT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "AspNetUserLogins" ALTER COLUMN "ProviderKey" TYPE text;
    ALTER TABLE "AspNetUserLogins" ALTER COLUMN "ProviderKey" SET NOT NULL;
    ALTER TABLE "AspNetUserLogins" ALTER COLUMN "ProviderKey" DROP DEFAULT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "AspNetUserLogins" ALTER COLUMN "LoginProvider" TYPE text;
    ALTER TABLE "AspNetUserLogins" ALTER COLUMN "LoginProvider" SET NOT NULL;
    ALTER TABLE "AspNetUserLogins" ALTER COLUMN "LoginProvider" DROP DEFAULT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    CREATE INDEX "IX_Cars_DriverId" ON "Cars" ("DriverId");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    CREATE INDEX "IX_Bookings_AssignedCarId" ON "Bookings" ("AssignedCarId");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    CREATE INDEX "IX_Bookings_DriverId" ON "Bookings" ("DriverId");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    CREATE INDEX "IX_Bookings_PassengerId" ON "Bookings" ("PassengerId");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" ADD CONSTRAINT "FK_Bookings_Cars_AssignedCarId" FOREIGN KEY ("AssignedCarId") REFERENCES "Cars" ("CarId") ON DELETE CASCADE;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" ADD CONSTRAINT "FK_Bookings_AspNetUsers_DriverId" FOREIGN KEY ("DriverId") REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Bookings" ADD CONSTRAINT "FK_Bookings_AspNetUsers_PassengerId" FOREIGN KEY ("PassengerId") REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    ALTER TABLE "Cars" ADD CONSTRAINT "FK_Cars_AspNetUsers_DriverId" FOREIGN KEY ("DriverId") REFERENCES "AspNetUsers" ("Id") ON DELETE RESTRICT;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20210412065410_UpdatedUserIds') THEN
    INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
    VALUES ('20210412065410_UpdatedUserIds', '3.1.13');
    END IF;
END $$;
