﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PFCAssignment.DataAccess.Interfaces;
using PFCAssignment.Domain;
using PFCAssignment.Utilities.Interfaces;

namespace PFCAssignment.Controllers
{
    [Authorize(Roles = "Driver")]
    public class CarsController : Controller
    {
        private readonly ICarsRepository _repo;
        private readonly IConfiguration _config;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogHandler _log;

        public CarsController(ICarsRepository repo, IConfiguration config,
            UserManager<IdentityUser> userManager, ILogHandler log)
        {
            _repo = repo;
            _config = config;
            _userManager = userManager;
            _log = log;
        }

        public ActionResult Details()
        {
            try
            {
                if (TempData["error"] != string.Empty)
                {
                    ViewBag.Error = TempData["error"];
                }

                if (TempData["message"] != string.Empty)
                {
                    ViewBag.Message = TempData["message"];
                }

                var userName = User.Identity.Name;

                return View(_repo.GetCar(userName));
            }
            catch
            {
                string message = "Error occured  when attempting " +
                    "to load driver's cars from db";
                _log.Log(message, Google.Cloud.Logging.Type.LogSeverity.Error);

                ViewBag.Error = message;

                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                string bucketName = _config.GetSection("AppSettings").GetSection("PhotosBucket").Value;
                string stringToReplace = $"https://storage.googleapis.com/{bucketName}/";
                var carPhoto = _repo.GetCar(id).Photo;
                string filename = carPhoto.Replace(stringToReplace,"");

                _repo.Delete(id);

                var storage = StorageClient.Create();
                storage.DeleteObject(bucketName, filename, null);

                TempData["messsage"] = "Car was successfully deleted";
                _log.Log($"Car with previous id {id} and its respective image " +
                    "were deleted", Google.Cloud.Logging.Type.LogSeverity.Info);

            }
            catch
            {
                string message = "Error occured when attempting to delete car";
                _log.Log(message, Google.Cloud.Logging.Type.LogSeverity.Error);

                TempData["error"] = message;
            }
            return RedirectToAction(nameof(Details));
        }

        public ActionResult Create()
        {
            return View();
        }

        // TODO: Test
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormFile carPhoto, Car newCar)
        {
            try
            {
                string bucketName = _config.GetSection("AppSettings").GetSection("PhotosBucket").Value;

                string filename = Guid.NewGuid() + Path.GetExtension(carPhoto.FileName);

                newCar.Photo = $"https://storage.googleapis.com/{bucketName}/{filename}";

                var storage = StorageClient.Create();
                using (var fileToUpload = carPhoto.OpenReadStream())
                {
                    storage.UploadObject(bucketName, filename, null, fileToUpload);
                }

                var driver = _userManager.GetUserAsync(User).Result;
                newCar.Driver = driver;
                newCar.DriverId = driver.Id;

                if (!ModelState.IsValid)
                    throw new ArgumentException();

                _repo.AddCar(newCar);

                TempData["message"] = "Car was successfully registered";

                return RedirectToAction(nameof(Details));
            }
            catch
            {
                string message = "An error occured when attempting to register " +
                    "a new car";
                _log.Log(message, Google.Cloud.Logging.Type.LogSeverity.Error);

                ViewBag.Error = message;

                return View();
            }
        }
    }
}
