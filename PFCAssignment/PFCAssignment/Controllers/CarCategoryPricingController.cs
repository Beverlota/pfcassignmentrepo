﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PFCAssignment.DataAccess.Interfaces;
using PFCAssignment.Models;
using PFCAssignment.Utilities.Interfaces;

namespace PFCAssignment.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CarCategoryPricingController : Controller
    {
        private readonly ICarCatCacheRepository _repo;
        private readonly ILogHandler _log;

        public CarCategoryPricingController(ICarCatCacheRepository repo, ILogHandler log)
        {
            _repo = repo;
            _log = log;
        }

        public IActionResult Index()
        {
            try
            {
                if (TempData["error"] != string.Empty)
                {
                    ViewBag.Error = TempData["error"];
                }

                if (TempData["message"] != string.Empty)
                {
                    ViewBag.Message = TempData["message"];
                }

                return View(_repo.GetPrices());
            }
            catch
            {
                _log.Log("Error occured when attempting to retreive prices from cache.", 
                    Google.Cloud.Logging.Type.LogSeverity.Error);

                ViewBag.Error = "Error occured when attempting to load " +
                    "prices ";
                return View();
            }
        }

        public IActionResult UpdatePrice()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdatePrice(CarCategoryResponse carCategoryResponse)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Model was not valid");

                _repo.UpdatePrice(carCategoryResponse);


                TempData["message"] = $"Successfully updated price of {carCategoryResponse.CarCategoryType.ToString().ToLower()} category";

                _log.Log($"User {User.Identity.Name} has updated the price of category {carCategoryResponse.CarCategoryType}.",
                    Google.Cloud.Logging.Type.LogSeverity.Info);

                return RedirectToAction("Index");
            }
            catch
            {
                _log.Log($"Error occured when attempting to update the price for category {carCategoryResponse.CarCategoryType}.",
                    Google.Cloud.Logging.Type.LogSeverity.Error);

                TempData["error"] = "Error occured when attempting to update the prices " +
                    $"for a category.";
            }

            return View();
        }
    }
}
