﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using PFCAssignment.Models;
using PFCAssignment.Utilities.Implementations;
using PFCAssignment.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PFCAssignment.Controllers
{
    public class CronController : Controller
    {
        private readonly Utilities.Interfaces.IEmailSender _emailSender;
        private readonly ILogHandler _log;
        private readonly ICacheHandler _cacheHandler;

        public CronController(Utilities.Interfaces.IEmailSender emailSender, ILogHandler log, ICacheHandler cacheHandler)
        {
            _emailSender = emailSender;
            _log = log;
            _cacheHandler = cacheHandler;
        }

        public void TriggerSendEmailCron()
        {
            try
            {
                if (!_cacheHandler.GetBookingDataFromCache().Any())
                    return;

                foreach(var bookingCacheEntry in _cacheHandler.GetBookingDataFromCache())
                {
                    _cacheHandler.RemoveEntryFromBookingCachedData(bookingCacheEntry);

                    int bookingId = bookingCacheEntry.Key;
                    CreateBookingRequest createBookingRequest = bookingCacheEntry.Value;

                    string emailContent = new ReceiptTemplate().buildReceiptTemplate(bookingId,
                        createBookingRequest.Passenger.UserName, createBookingRequest.TripPrice);

                    // Triggering cloud function to send the email
                    dynamic functionOutput = _emailSender.TriggerCloudFunction(emailContent, createBookingRequest.Passenger.Email);

                    if (functionOutput.StatusCode != HttpStatusCode.OK)
                        throw new ApplicationException(functionOutput.Message);

                    _log.Log("Successfully sent email receipt to passenger.",
                        Google.Cloud.Logging.Type.LogSeverity.Info);
                }
            }
            catch
            {
                _log.Log("Error occured when attempting to send email via cron job", Google.Cloud.Logging.Type.LogSeverity.Error);
            }

        }
    }
}
