﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PFCAssignment.DataAccess.Interfaces;
using PFCAssignment.Domain;
using PFCAssignment.Models;
using PFCAssignment.Utilities.Implementations;
using PFCAssignment.Utilities.Interfaces;

namespace PFCAssignment.Controllers
{
    public class BookingsController : Controller
    {
        private readonly IBookingsRepository _repo;
        private readonly ICarsRepository _carRepo;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IPubSubRepository _pubSubRepository;
        private readonly ILogHandler _log;
        private readonly ICarCatCacheRepository _cacheRepo;
        private readonly ICacheHandler _cacheHandler;

        public BookingsController(IBookingsRepository repo,
            UserManager<IdentityUser> userManager, ICarsRepository carRepo,
            IPubSubRepository pubSubRepository,
            ILogHandler log,
            ICarCatCacheRepository cacheRepo,
            ICacheHandler cacheHandler)
        {
            _repo = repo;
            _userManager = userManager;
            _carRepo = carRepo;
            _pubSubRepository = pubSubRepository;
            _log = log;
            _cacheRepo = cacheRepo;
            _cacheHandler = cacheHandler;
        }

        /// <summary>
        /// Returns acknowledged bookings from database. Operation is available
        /// only for admins
        /// </summary>
        [Authorize(Roles = "Admin")]
        public ActionResult GetAcknowledgedBookings()
        {
            try
            {

                var displayData = _repo.GetAcknowledgedBookings();

                string message = $"User {User.Identity.Name}" +
                    $" requested all acknowledged bookings from the database.";

                _log.Log(message, Google.Cloud.Logging.Type.LogSeverity.Info);

                return View(displayData);
            }
            catch
            {
                string message = "Error occured when admin attempted to retreive acknowledged" +
                    $"bookings from db. ";
                _log.Log(message, Google.Cloud.Logging.Type.LogSeverity.Error);

                TempData["error"] = "Error occured when attempting to get acknowledged bookings";
                return View(new List<Booking>());
            }
        }

        /// <summary>
        /// Loads the first available job from the respective car topic
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Driver")]
        public ActionResult GetAvailableJobs()
        {
            try
            {
                if (TempData["error"] != string.Empty)
                {
                    ViewBag.Error = TempData["error"];
                }

                if (TempData["message"] != string.Empty)
                {
                    ViewBag.Message = TempData["message"];
                }

                //select from which topic to request from
                string carCategory = _carRepo.GetCar(User.Identity.Name)?.CarCategory.ToString();

                // load first message from respective topic
                var firstMessageResponse = _pubSubRepository.GetFirstMessage(carCategory);
                string firstMessage = firstMessageResponse.Item1;

                CreateBookingRequest output = JsonConvert.DeserializeObject<CreateBookingRequest>(firstMessage);

                if (output == null)
                {
                    ViewBag.Message = "No available jobs were found";
                    _log.Log($"No bookings were loaded from the topic {carCategory}",
                        Google.Cloud.Logging.Type.LogSeverity.Info);
                }


                if (output != null)
                {
                    ViewBag.AckIds = JsonConvert.SerializeObject(firstMessageResponse.Item2);
                    ViewBag.Request = JsonConvert.SerializeObject(output);
                }


                string message = $"User {User.Identity.Name}" +
                   $" requested the first available booking in the pubsub.";

                _log.Log(message, Google.Cloud.Logging.Type.LogSeverity.Info);

                return View(output);
            }
            catch
            {
                _log.Log("Error occured when admin attempted to retreive first" +
                   $"available job from pub sub.", Google.Cloud.Logging.Type.LogSeverity.Error);

                TempData["error"] = "Error occured when attempting to get available jobs";
                return View(new List<Booking>());
            }
        }

        /// <summary>
        /// Sets the supplied booking to acknowledge, downloads the details and redirects to the 
        /// available jobs page
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Driver")]
        public ActionResult Acknowledge(string createBookingRequestStr, string ackIds)
        {
            try
            {
                CreateBookingRequest createBookingRequest = JsonConvert.DeserializeObject<CreateBookingRequest>
                    (createBookingRequestStr);

                var driver = _userManager.GetUserAsync(User).Result;
                Car assignedCar = _carRepo.GetCar(User.Identity.Name);

                // acknowledge in pubsub
                _pubSubRepository.Acknowledge(assignedCar.CarCategory.ToString(), ackIds);

                string message = $"User {User.Identity.Name}" +
                    $" acknowledged a booking.";

                _log.Log(message, Google.Cloud.Logging.Type.LogSeverity.Info);

                //prep data for saving in db
                Booking booking = new Booking()
                {
                    PickUpAddress = createBookingRequest.PickUpAddress,
                    DropOffAddress = createBookingRequest.DropOffAddress,
                    PassengerId = createBookingRequest.Passenger.Id,
                    CarTypeRequested = createBookingRequest.CarType,
                    Acknowledged = true,
                    DriverId = driver.Id,
                    AssignedCar = assignedCar,
                    AssignedCarId = assignedCar.CarId
                };

                int bookingId = _repo.AddBooking(booking);

                if (bookingId == 0)
                {
                    throw new ApplicationException("Booking Id was left as 0. Could indicate" +
                        " that the booking was not inserted correctly");
                }

                _log.Log("Successfully inserted a new booking into the database.",
                    Google.Cloud.Logging.Type.LogSeverity.Info);


                //Adding entry to the cache so that cron job can send the email
                _cacheHandler.AddEntryToBookingCachedData(new KeyValuePair<int, CreateBookingRequest>(bookingId, createBookingRequest));

                //generating downloadable details
                dynamic downloadableDetails = new ExpandoObject();
                downloadableDetails.BookingId = bookingId;
                downloadableDetails.PickUpAddress = createBookingRequest.PickUpAddress;
                downloadableDetails.DropOffAddress = createBookingRequest.DropOffAddress;
                downloadableDetails.PassengerName = createBookingRequest.Passenger.UserName;

                var fileToDownload = Encoding.ASCII.GetBytes(
                    JsonConvert.SerializeObject(downloadableDetails));

                return File(fileToDownload, "text/plain", $"Booking{bookingId}Details.txt");
            }
            catch
            {
                _log.Log("Error occured when admin attempted to acknowledge booking from" +
                 $"pubsub or when downloading details.", Google.Cloud.Logging.Type.LogSeverity.Error);

                TempData["error"] = "Error occured when attempting to acknowledge " +
                    "booking";

                return RedirectToAction("GetAvailableJobs");
            }
        }

        [Authorize(Roles = "Passenger")]
        public ActionResult ConfirmBooking(CreateBookingRequest createBookingRequest)
        {
            return View(createBookingRequest);
        }


        [Authorize(Roles = "Passenger")]
        public ActionResult Create()
        {
            if (TempData["error"] != string.Empty)
            {
                ViewBag.Error = TempData["error"];
            }

            if (TempData["message"] != string.Empty)
            {
                ViewBag.Message = TempData["message"];
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Passenger")]
        public ActionResult Create(CreateBookingRequest createBookingRequest)
        {
            try
            {
                createBookingRequest.Passenger = _userManager.GetUserAsync(User).Result;
                createBookingRequest.TripPrice = _cacheRepo.GetPriceForCategory(createBookingRequest.CarType);

                TempData["request"] = JsonConvert.SerializeObject(createBookingRequest);
            }
            catch
            {
                _log.Log($"Error occured when user {User.Identity.Name} attempted to create a " +
                    $"new booking.",
                  Google.Cloud.Logging.Type.LogSeverity.Error);

                ViewBag.Error = "Error occured when attempting to create a new booking.";
            }

            return RedirectToAction("ConfirmBooking", createBookingRequest);
        }

        [Authorize(Roles = "Passenger")]
        public ActionResult InsertBooking()
        {
            try
            {
                CreateBookingRequest createBookingRequest = JsonConvert.DeserializeObject<CreateBookingRequest>((string)TempData["request"]);

                _pubSubRepository.PublishCreateBookingRequest(createBookingRequest);

                TempData["message"] = "Successfully added your new booking";

                _log.Log($"User {User.Identity.Name} created a new booking",
                    Google.Cloud.Logging.Type.LogSeverity.Info);
            }
            catch
            {
                _log.Log($"Error occured when user {User.Identity.Name} attempted to create a " +
                    $"new booking.",
                  Google.Cloud.Logging.Type.LogSeverity.Error);

                ViewBag.Error = "Error occured when attempting to create a new booking.";
            }

            return RedirectToAction("Create");
        }
    }
}
