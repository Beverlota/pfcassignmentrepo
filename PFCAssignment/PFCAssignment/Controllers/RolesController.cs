﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PFCAssignment.Utilities.Interfaces;

namespace PFCAssignment.Controllers
{
    public class RolesController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogHandler _log;

        public RolesController(UserManager<IdentityUser> userManager, 
            RoleManager<IdentityRole> roleManager, ILogHandler log)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _log = log;
        }

        public IActionResult ListRoles()
        {
            try
            {
                if (TempData["error"] != string.Empty)
                {
                    ViewBag.Error = TempData["error"];
                }

                if (TempData["message"] != string.Empty)
                {
                    ViewBag.Message = TempData["message"];
                }

                var roles = _roleManager.Roles.ToList();
                return View(roles);
            }
            catch
            {
                _log.Log("Error occured when attempting to retreive roles.",
                    Google.Cloud.Logging.Type.LogSeverity.Error);

                return View(new List<IdentityRole>() { });   
            }
        }

        public IActionResult Create()
        {
            return View(new IdentityRole());
        }

        [HttpPost]
        public async Task<IActionResult> Create(IdentityRole role)
        {
            try
            {
                await _roleManager.CreateAsync(role);

                _log.Log($"New role was created {role.Name}",
                   Google.Cloud.Logging.Type.LogSeverity.Info);
            }
            catch
            {
                _log.Log("Error occured when attempting to retreive roles.",
                   Google.Cloud.Logging.Type.LogSeverity.Error);

                TempData["error"] = "Error occured when attempting to create " +
                    "a new role";
            }

            return RedirectToAction("ListRoles");
        }

        public IActionResult UpdateUserRole()
        {
            ViewBag.Roles = _roleManager.Roles.Select(r => r.Name).ToList().Select(
               f => new SelectListItem { Text = f, Value = f });
            ViewBag.Users = _userManager.Users.Select(u => u.UserName).ToList().Select(
               f => new SelectListItem { Text = f, Value = f });

            return View();
        }

        [HttpPost]
        public IActionResult UpdateUserRole(string userName, string role)
        {
            try
            {
                var user = _userManager.FindByNameAsync(userName).Result;
                var currentRole = _userManager.GetRolesAsync(user).Result.First();

                var removal_result = _userManager.RemoveFromRoleAsync(user, currentRole).Result;

                if (removal_result.Succeeded)
                {
                    var insertion_result = _userManager.AddToRoleAsync(user, role).Result;

                    if (!insertion_result.Succeeded)
                        throw new Exception("Insertion of new role not successful");
                }
                else
                {

                    throw new Exception("Error occured when attempting to remove role");
                }

                TempData["message"] = "User Role Successfully updated";

                _log.Log($"Updated user {userName} to role {role}", Google.Cloud.Logging.Type.LogSeverity.Info);

                return RedirectToAction("ListRoles");
            }
            catch
            {

                string message = $"Error occured when attempting to update role of user {userName} to {role}";
                _log.Log(message, Google.Cloud.Logging.Type.LogSeverity.Error);

                TempData["error"] = message;

                return RedirectToAction("ListRoles");
            }
        }
    }
}
