﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Domain
{
    public class Booking
    {
        [Key]
        public int BookingId { get; set; }
        [Required]
        public string PickUpAddress { get; set; }
        [Required]
        public string DropOffAddress { get; set; }
        [DefaultValue(false)]
        public bool Acknowledged { get; set; }
        public CarCategory CarTypeRequested { get; set; }
        [ForeignKey("Car")]
        public int AssignedCarId { get; set; }
        public virtual Car AssignedCar { get; set; }
        [ForeignKey("IdentityUser")]
        public string PassengerId { get; set; }
        public virtual IdentityUser Passenger { get; set; }
        [ForeignKey("IdentityUser")]
        public string DriverId { get; set; }
        public virtual IdentityUser Driver { get; set; }
    }
}
