﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.Domain
{
    public class Car
    {
        [Key]
        public int CarId { get; set; }
        [DisplayName("Passenger Capacity")]
        public int PassengerCapacity { get; set; }
        [DisplayName("Car Category Type")]
        public CarCategory CarCategory { get; set; }
        [DisplayName("Car Condition")]
        public CarCondition Condition { get; set; }
        [DisplayName("Registration Plate")]
        public string RegistrationPlate { get; set; }
        [DisplayName("Has  Air Conditioning")]
        public bool HasAC { get; set; }
        public string Photo { get; set; }
        [DisplayName("Has  Wifi")]
        public bool HasWifi { get; set; }
        [DisplayName("Food and Drinks Allowed")]
        public bool AllowsFoodDrink { get; set; }
        [ForeignKey("IdentityUser")]
        public string DriverId { get; set; }
        public virtual IdentityUser Driver { get; set; }
    }
}
public enum CarCategory
{
    ECONOMY,
    BUSINESS,
    LUXURY
}

public enum CarCondition{
    EXCELLENT,
    VERYGOOD,
    GOOD,
    FAIR
}
