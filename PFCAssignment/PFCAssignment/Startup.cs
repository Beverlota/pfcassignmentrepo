using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using PFCAssignment.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PFCAssignment.DataAccess.Interfaces;
using PFCAssignment.DataAccess.Repositories;
using DinkToPdf.Contracts;
using DinkToPdf;
using Microsoft.AspNetCore.Http;
using Google.Cloud.SecretManager.V1;
using Newtonsoft.Json;
using Google.Cloud.Diagnostics.AspNetCore;
using PFCAssignment.Utilities.Interfaces;
using PFCAssignment.Utilities.Implementations;

namespace PFCAssignment
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostEnvironment host)
        {
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS",
                host.ContentRootPath + @"/pfcassignment-27893b509960.json");

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public ISecretManagerHandler SecretManagerHandler;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.Configure<CookiePolicyOptions>(options =>
			{
				options.MinimumSameSitePolicy = SameSiteMode.Unspecified;
				options.OnAppendCookie = cookieContext => 
					CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
				options.OnDeleteCookie = cookieContext => 
					CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
			});
			
            services.AddScoped<ISecretManagerHandler, SecretManagerHandler>();
            services.AddScoped<ILogHandler, LogHandler>();
            services.AddScoped<IBookingsRepository, BookingsRepository>();
            services.AddScoped<ICarsRepository, CarsRepository>();
            services.AddScoped<ICarCatCacheRepository, CarCategoryCachingRepo>();
            services.AddScoped<ITopicHandler, TopicHandler>();
            services.AddScoped<IPubSubRepository, PubSubRepository>();
            services.AddScoped<IEmailSender, EmailSender>();
            services.AddScoped<ICacheHandler, CacheHandler>();

            SecretManagerHandler = new SecretManagerHandler(Configuration);

            string connectionStringLessPassword = Configuration.GetConnectionString("DefaultConnection");
            string dbPassword = SecretManagerHandler.GetSecretValue("PostgresPassword");
            
            //add default connection string type stuff 
            services.AddDbContext<ApplicationDbContext>(options =>
            options.UseNpgsql(
                connectionStringLessPassword+dbPassword)
                );

            services.AddIdentity<IdentityUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = true)
                 .AddDefaultUI()
                 .AddEntityFrameworkStores<ApplicationDbContext>()
                 .AddDefaultTokenProviders();
            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddAuthentication()
            .AddGoogle(options =>
            {
                //IConfigurationSection googleAuthNSection =
                //    Configuration.GetSection("Authentication:Google");

                options.ClientId = SecretManagerHandler.GetSecretValue("Authentication:Google:ClientId");
                options.ClientSecret = SecretManagerHandler.GetSecretValue("Authentication:Google:ClientSecret");
            });

            services.AddGoogleExceptionLogging(options =>
            {
                options.ProjectId = Configuration.GetSection("AppSettings").GetSection("ProjectId").Value;
                options.ServiceName = "PFC2021AssignmentService";
                options.Version = "0.01";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseGoogleExceptionLogging();
            app.UseStaticFiles();

            app.UseRouting();
			
			app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    
		private void CheckSameSite(HttpContext httpContext, CookieOptions options)
		{
			if (options.SameSite == SameSiteMode.None)
			{
				var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
				options.SameSite = SameSiteMode.Unspecified;
			}
		}
	}
}
