﻿using PFCAssignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Interfaces
{
    public interface ICarsRepository
    {
        public Car GetCar(int carId);
        public Car GetCar(string username);
        public void AddCar(Car car);
        public  IQueryable<Car> GetAllCars();
        public void Delete(int carId);
    }
}
