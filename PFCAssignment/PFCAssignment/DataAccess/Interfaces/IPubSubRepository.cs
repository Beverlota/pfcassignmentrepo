﻿using PFCAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Interfaces
{
    public interface IPubSubRepository
    {
        public void PublishCreateBookingRequest(CreateBookingRequest createBookingRequest);
        public Tuple<string, List<string>> GetFirstMessage(string carCategory);
        public void Acknowledge(string driverCarCategory, string ackIds);
    }
}
