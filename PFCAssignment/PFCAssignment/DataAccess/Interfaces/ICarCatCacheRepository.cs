﻿using PFCAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Interfaces
{
    public interface ICarCatCacheRepository
    {
        public List<CarCategoryResponse> GetPrices();
        public decimal GetPriceForCategory(CarCategory carCategory);
        public void UpdatePrice(CarCategoryResponse carCategoryResponseUpdated);
    }
}
