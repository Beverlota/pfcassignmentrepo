﻿using PFCAssignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Interfaces
{
    public interface IBookingsRepository
    {
        public IQueryable<Booking> GetAllBookings();
        public List<Booking> GetAcknowledgedBookings();
        public List<Booking> GetUnacknowledgedBookings();
        public Booking GetBooking(int bookingId);
        public void UpdateBooking(Booking booking);
        public int AddBooking(Booking booking);
    }
}
