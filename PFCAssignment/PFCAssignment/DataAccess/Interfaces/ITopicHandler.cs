﻿using Google.Cloud.PubSub.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Interfaces
{
    public interface ITopicHandler
    {
        public Topic CreateTopic(string topicId);
    }
}
