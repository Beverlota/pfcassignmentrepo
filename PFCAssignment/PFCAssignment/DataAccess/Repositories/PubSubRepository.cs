﻿using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PFCAssignment.DataAccess.Interfaces;
using PFCAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Repositories
{
    public class PubSubRepository : IPubSubRepository
    {
        private readonly ITopicHandler _topicHandler;
        private readonly IConfiguration _config;

        public PubSubRepository(ITopicHandler topicHandler, IConfiguration config){
            _topicHandler = topicHandler;
            _config = config;
        }

        public Tuple<string, List<string>> GetFirstMessage(string driverCarCategory)
        {
            string projectId = _config.GetSection("AppSettings").GetSection("ProjectId").Value;
            string subscriptionId = _config.GetSection("AppSettings").GetSection("SubscriberIds").
                GetSection(driverCarCategory).Value;

            SubscriptionName subscriptionName = SubscriptionName.FromProjectSubscription(projectId, subscriptionId);
            SubscriberServiceApiClient subscriberClient = SubscriberServiceApiClient.Create();

            try
            {
                PullResponse response = subscriberClient.Pull(subscriptionName, returnImmediately: true, maxMessages: 1);

                if (response.ReceivedMessages.Count > 1)
                    throw new ApplicationException("Error occured more then 1 message was returned");

                if (response.ReceivedMessages.Count == 0)
                    return new Tuple<string, List<string>>(string.Empty, new List<string>());

                string messageContents = response.ReceivedMessages[0].Message.Data.ToStringUtf8();
                IEnumerable<string> ackIds = response.ReceivedMessages.Select(r=> r.AckId);

                // log extracted message

                return new Tuple<string, List<string>>(messageContents, ackIds.ToList());
            }
            catch (RpcException ex) when (ex.Status.StatusCode == StatusCode.Unavailable)
            {
                throw new ApplicationException("too many concurrent pull requests pending for the given subscription.");
                //log error
            }
        }

        public void Acknowledge(string driverCarCategory, string unparsedAckIds)
        {
            List<string> ackIds = JsonConvert.DeserializeObject<List<string>>(unparsedAckIds);

            string projectId = _config.GetSection("AppSettings").GetSection("ProjectId").Value;
            string subscriptionId = _config.GetSection("AppSettings").GetSection("SubscriberIds").
                GetSection(driverCarCategory).Value;

            SubscriptionName subscriptionName = SubscriptionName.FromProjectSubscription(projectId, subscriptionId);
            SubscriberServiceApiClient subscriberClient = SubscriberServiceApiClient.Create();

            try
            {
                subscriberClient.Acknowledge(subscriptionName, ackIds);
            }
            catch (RpcException ex) when (ex.Status.StatusCode == StatusCode.Unavailable)
            {
                throw new ApplicationException("too many concurrent pull requests pending for the given subscription.");
                //log error
            }
        }


        public void PublishCreateBookingRequest(CreateBookingRequest createBookingRequest)
        {
            Topic t = _topicHandler.CreateTopic(createBookingRequest.CarType.ToString().ToLower());

            TopicName topicName = t.TopicName;
            string serialisedCreateBookingRequest = JsonConvert.SerializeObject(createBookingRequest);
            Task<PublisherClient> task = PublisherClient.CreateAsync(topicName);
            task.Wait();
            PublisherClient publisher = task.Result;


            var pubsubMessage = new PubsubMessage
            {
                Data = ByteString.CopyFromUtf8(serialisedCreateBookingRequest
                    )
            };

            Task<string> taskP = publisher.PublishAsync(pubsubMessage);
            taskP.Wait();
            string message = taskP.Result;

            //log message
        }
    }
}
