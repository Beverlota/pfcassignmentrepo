﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PFCAssignment.DataAccess.Interfaces;
using PFCAssignment.Models;
using PFCAssignment.Utilities.Interfaces;
using StackExchange.Redis;

namespace PFCAssignment.DataAccess.Repositories
{
    public class CarCategoryCachingRepo : ICarCatCacheRepository
    {
        private IDatabase db;
        private readonly string carCategoryResponse_key;

        public CarCategoryCachingRepo(IConfiguration config, ISecretManagerHandler secretManagerHandler)
        {
            string connectionStringLessPassword = config.GetConnectionString("cachedb");
            string cacheDBConnectionPassword = secretManagerHandler.GetSecretValue("CachingDBPassword");
            var connectionString = connectionStringLessPassword+cacheDBConnectionPassword;
            var cm = ConnectionMultiplexer.Connect(connectionString);

            db = cm.GetDatabase();
            carCategoryResponse_key = config.GetSection("AppSettings").
                GetSection("CatPriceCacheKey").Value;
        }

        private void GenerateAndInsertDefaultValues()
        {
            List<CarCategoryResponse> carCategoryResponses = new List<CarCategoryResponse>()
            {
                new CarCategoryResponse(){CarCategoryType=CarCategory.ECONOMY, Price=1.3m},
                new CarCategoryResponse(){CarCategoryType=CarCategory.BUSINESS, Price=2.3m},
                new CarCategoryResponse(){CarCategoryType=CarCategory.LUXURY, Price=3.3m}
            };

            UpdateCarCategoryResponsesInCache(carCategoryResponses);
        }

        public List<CarCategoryResponse> GetPrices()
        {
            if (!db.KeyExists(carCategoryResponse_key))
                GenerateAndInsertDefaultValues();

            var result = JsonConvert.DeserializeObject<List<CarCategoryResponse>>(
                db.StringGet(carCategoryResponse_key)
                    );

            if (!result.Any())
            {
                GenerateAndInsertDefaultValues();
                GetPrices();
            }

            return result;
        }

        public decimal GetPriceForCategory(CarCategory carCategory)
        {
            return GetPrices().First(p => p.CarCategoryType == carCategory).Price;
        }

        public void UpdatePrice(CarCategoryResponse carCategoryResponseUpdated)
        {
            List<CarCategoryResponse> cachedList = GetPrices();

            var listItem_toUpdate = cachedList.First(c => c.CarCategoryType == carCategoryResponseUpdated.CarCategoryType);

            cachedList.Remove(listItem_toUpdate);
            cachedList.Add(carCategoryResponseUpdated);

            UpdateCarCategoryResponsesInCache(cachedList);
        }

        private void UpdateCarCategoryResponsesInCache(List<CarCategoryResponse> updatedList)
        {
            var serialised_carCategoryResponses = JsonConvert.SerializeObject(updatedList);

            db.StringSet(carCategoryResponse_key,
                serialised_carCategoryResponses
                );
        }
    }
}
