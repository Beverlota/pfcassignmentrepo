﻿using Microsoft.EntityFrameworkCore;
using PFCAssignment.Data;
using PFCAssignment.DataAccess.Interfaces;
using PFCAssignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Repositories
{
    public class CarsRepository : ICarsRepository
    {
        private readonly ApplicationDbContext _context;

        public CarsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddCar(Car car)
        {
            _context.Cars.Add(car);
            _context.SaveChanges();
        }

        public IQueryable<Car> GetAllCars()
        {
            return _context.Cars;
        }

        public Car GetCar(int carId)
        {
            return GetAllCars().FirstOrDefault(c=>c.CarId == carId);
        }

        public void Delete(int carId)
        {
            _context.Cars.Remove(_context.Cars.First(c => c.CarId==carId));

            _context.SaveChanges();
        }

        public Car GetCar(string username)
        {
            return GetAllCars().FirstOrDefault(c => c.Driver.UserName.Equals(username));
        }
    }
}
