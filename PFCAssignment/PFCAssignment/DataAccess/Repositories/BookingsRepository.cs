﻿using PFCAssignment.Data;
using PFCAssignment.DataAccess.Interfaces;
using PFCAssignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Repositories
{
    public class BookingsRepository : IBookingsRepository
    {
        private readonly ApplicationDbContext _context;

        public BookingsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void UpdateBooking(Booking booking)
        {
            _context.Bookings.Update(booking);
            _context.SaveChanges();
        }

        public int AddBooking(Booking booking)
        {
            int bookingId = 0;

            using (var context = new ApplicationDbContext())
            {
                _context.Bookings.Add(booking);
                _context.SaveChanges();
            }

            bookingId = _context.Bookings.ToList().Select(b => b.BookingId).Max();

            return bookingId;
        }

        public List<Booking> GetAcknowledgedBookings()
        {
            return GetAllBookings().Where(b=>b.Acknowledged == true).ToList();
        }

        public IQueryable<Booking> GetAllBookings()
        {
            return _context.Bookings;
        }

        public Booking GetBooking(int bookingId)
        {
            return GetAllBookings().FirstOrDefault(b => b.BookingId == bookingId);
        }

        public List<Booking> GetUnacknowledgedBookings()
        {
            return GetAllBookings().Where(b => b.Acknowledged == false).ToList();
        }
    }
}
