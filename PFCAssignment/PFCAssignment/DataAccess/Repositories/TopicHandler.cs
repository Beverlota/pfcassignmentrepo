﻿
using Google.Cloud.PubSub.V1;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using PFCAssignment.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFCAssignment.DataAccess.Repositories
{
    public class TopicHandler : ITopicHandler
    {
        IConfiguration _config;
        private readonly string _projectId;

        public TopicHandler(IConfiguration config)
        { 
            _config = config;
            _projectId = _config.GetSection("AppSettings").
                GetSection("ProjectId").Value;
        }

        public Topic CreateTopic(string carCategoryName)
        {
            string topicId = _config.GetSection("AppSettings").
                GetSection("TopicIds").GetSection(carCategoryName).Value;
            PublisherServiceApiClient publisher = PublisherServiceApiClient.Create();
            var topicName = TopicName.FromProjectTopic(_projectId, topicId);
            Topic topic = null;

            try
            {
                topic = publisher.GetTopic(topicName);

            }
            catch (RpcException e) when (e.Status.StatusCode == StatusCode.NotFound)
            {
                topic = publisher.CreateTopic(topicName);
            }
            return topic;
        }
    }
}
